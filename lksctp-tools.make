# -*-makefile-*-
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_LKSCTP_TOOLS) += lksctp-tools

#
# Paths and names
#
LKSCTP_TOOLS_VERSION        := 1.0.16
LKSCTP_TOOLS_PREFIX         := lksctp-tools
LKSCTP_TOOLS                := $(LKSCTP_TOOLS_PREFIX)-$(LKSCTP_TOOLS_VERSION)
LKSCTP_TOOLS_SUFFIX         := tar.gz
LKSCTP_TOOLS_URL            := http://sourceforge.net/projects/lksctp/files/lksctp-tools/$(LKSCTP_TOOLS).$(LKSCTP_TOOLS_SUFFIX)
LKSCTP_TOOLS_SOURCE         := $(SRCDIR)/$(LKSCTP_TOOLS).$(LKSCTP_TOOLS_SUFFIX)
LKSCTP_TOOLS_DIR            := $(BUILDDIR)/$(LKSCTP_TOOLS)
LKSCTP_TOOLS_ARCH           := linux

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(LKSCTP_TOOLS_SOURCE):
	@$(call targetinfo)
	@$(call get, LKSCTP_TOOLS)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

$(STATEDIR)/lksctp-tools.extract:
	@$(call targetinfo)
	@$(call clean, $(LKSCTP_TOOLS_DIR))
	mkdir -p $(LKSCTP_TOOLS_DIR)
	tar -C $(LKSCTP_TOOLS_DIR) -xvf $(LKSCTP_TOOLS_SOURCE)
	@$(call patchin, LKSCTP_TOOLS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

LKSCTP_TOOLS_PATH	:= PATH=$(CROSS_PATH)
LKSCTP_TOOLS_ENV 	:= $(CROSS_ENV)

#
# autoconf
#
LKSCTP_TOOLS_AUTOCONF := $(CROSS_AUTOCONF_USR)

$(STATEDIR)/lksctp-tools.prepare:
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/lksctp-tools.compile:
	@$(call targetinfo, $@)

	cd $(LKSCTP_TOOLS_DIR)/$(LKSCTP_TOOLS) && \
	        ./bootstrap $(PARALLELMFLAGS)
	cd $(LKSCTP_TOOLS_DIR)/$(LKSCTP_TOOLS) && \
		./configure $(LKSCTP_TOOLS_AUTOCONF)
	cd $(LKSCTP_TOOLS_DIR)/$(LKSCTP_TOOLS) && \
		$(LKSCTP_TOOLS_PATH) $(LKSCTP_TOOLS_ENV) make $(PARALLELMFLAGS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/lksctp-tools.install:
	@$(call targetinfo)
	@mkdir -p $(PTXCONF_SYSROOT_TARGET)/usr/include/netinet
	@cp -f $(LKSCTP_TOOLS_DIR)/$(LKSCTP_TOOLS)/src/include/netinet/sctp.h $(PTXCONF_SYSROOT_TARGET)/usr/include/netinet/
	@cp -f $(LKSCTP_TOOLS_DIR)/$(LKSCTP_TOOLS)/src/lib/.libs/libsctp.so.1.0.16 $(PTXCONF_SYSROOT_TARGET)/usr/lib
	@ln -s $(PTXCONF_SYSROOT_TARGET)/usr/lib/libsctp.so.1.0.16 \
		$(PTXCONF_SYSROOT_TARGET)/usr/lib/libsctp.so.1
	@ln -s $(PTXCONF_SYSROOT_TARGET)/usr/lib/libsctp.so.1.0.16 \
		$(PTXCONF_SYSROOT_TARGET)/usr/lib/libsctp.so
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/lksctp-tools.targetinstall:
	@$(call targetinfo, $@)

	@$(call install_init, lksctp-tools)
	@$(call install_fixup, lksctp-tools,PACKAGE,lksctp-tools)
	@$(call install_fixup, lksctp-tools,PRIORITY,optional)
	@$(call install_fixup, lksctp-tools,VERSION,$(LKSCTP_TOOLS_VERSION))
	@$(call install_fixup, lksctp-tools,SECTION,base)
	@$(call install_fixup, lksctp-tools,AUTHOR,"http://lksctp.sourceforge.net/")
	@$(call install_fixup, lksctp-tools,DEPENDS,)
	@$(call install_fixup, lksctp-tools,DESCRIPTION,"lksctp-tool-install")


	@$(call install_copy, lksctp-tools, 0, 0, 0644, \
		$(LKSCTP_TOOLS_DIR)/$(LKSCTP_TOOLS)/src/lib/.libs/libsctp.so.1.0.16, \
		/usr/lib/libsctp.so.1.0.16, y)

	@$(call install_link, lksctp-tools, \
		libsctp.so.1.0.16, \
		/usr/lib/libsctp.so.1)

	@$(call install_link, lksctp-tools, \
		libsctp.so.1.0.16, \
		/usr/lib/libsctp.so)

	@$(call install_finish, lksctp-tools)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

lksctp-tools_clean:
	rm -rf $(STATEDIR)/lksctp-tools.*
	rm -rf $(PKGDIR)/lksctp-tools_*
	rm -rf $(LKSCTP_TOOLS_DIR)
	rm -rf $(PTXCONF_SYSROOT_TARGET)/usr/lib/libsctp.*

# vim: syntax=make
