# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_LIBSELINUX) += host-libselinux

# Paths and names
HOST_LIBSELINUX_VERSION := 1.0
HOST_LIBSELINUX         := libselinux-$(HOST_LIBSELINUX_VERSION)
# path to directory containing source files
HOST_LIBSELINUX_SRCDIR  := $(PTXDIST_WORKSPACE)/../opensource/libselinux
HOST_LIBSELINUX_URL     := file://$(HOST_LIBSELINUX_SRCDIR)
HOST_LIBSELINUX_DIR     := $(HOST_BUILDDIR)/$(HOST_LIBSELINUX)

LIBSELINUX_MK = $(HOST_LIBSELINUX_DIR)/Makefile

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-libselinux.get: $(host-libselinux_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

$(STATEDIR)/host-libselinux.extract: $(host-libselinux_extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_LIBSELINUX_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

HOST_LIBSELINUX_MAKEVARS += $(PARALLELMFLAGS)

host-libselinux_prepare: $(STATEDIR)/host-libselinux.prepare

$(STATEDIR)/host-libselinux.prepare: $(host-libselinux_prepare_deps_default)
	@$(call targetinfo, $@)
	@echo 'CC = $(HOSTCC)' > $(LIBSELINUX_MK)
	@echo 'AR = ar' >> $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@echo 'CFLAGS += -O2 -Wall -g -I$$(SRCDIR)/include' >> $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@echo 'vpath %.c $$(SRCDIR)/src' >> $(LIBSELINUX_MK)
	@echo 'vpath %.h $$(SRCDIR)/include' >> $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@echo 'libselinux_src_files := callbacks.c check_context.c freecon.c init.c label.c label_file.c label_android_property.c' >> $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@echo 'libselinux_src_obj := $$(libselinux_src_files:.c=.o)' >>  $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@echo 'all: libselinux.a' >> $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@echo 'libselinux.a: $$(libselinux_src_obj)' >> $(LIBSELINUX_MK)
	@echo '	$$(AR) crsP $$@ $$^' >> $(LIBSELINUX_MK)
	@echo '' >> $(LIBSELINUX_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-libselinux_compile: $(STATEDIR)/host-libselinux.compile

$(STATEDIR)/host-libselinux.compile: $(host-libselinux_compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_LIBSELINUX_PATH) $(MAKE) -C $(HOST_LIBSELINUX_DIR) SRCDIR=$(HOST_LIBSELINUX_SRCDIR) $(HOST_LIBSELINUX_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-libselinux_install: $(STATEDIR)/host-libselinux.install

$(STATEDIR)/host-libselinux.install: $(host-libselinux_install_deps_default)
	@$(call targetinfo, $@)
	@-cp -f $(HOST_LIBSELINUX_DIR)/libselinux.a $(PTXCONF_SYSROOT_HOST)/lib
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/include/selinux
	@-cp -f $(HOST_LIBSELINUX_SRCDIR)/include/selinux/* \
		$(PTXCONF_SYSROOT_HOST)/include/selinux/
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------
host-libselinux_clean: $(STATEDIR)/host-libselinux.clean
$(STATEDIR)/host-libselinux.clean:
	rm -rf $(STATEDIR)/host-libselinux.*
	rm -rf $(HOST_LIBSELINUX_DIR)
