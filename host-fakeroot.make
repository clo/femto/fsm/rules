# -*-makefile-*-
#
# Copyright (C) 2003 by Benedikt Spranger
#               2010 by Marc Kleine-Budde <mkl@pengutronix.de>
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
HOST_PACKAGES-$(PTXCONF_HOST_FAKEROOT) += host-fakeroot

#
# Paths and names
#
HOST_FAKEROOT_VERSION	:= 1.14.4
HOST_FAKEROOT_MD5	:= bea628be77838aaa7323a2f7601c2d7e
HOST_FAKEROOT_SUFFIX	:= tar.bz2
HOST_FAKEROOT		:= fakeroot-$(HOST_FAKEROOT_VERSION)
HOST_FAKEROOT_TARBALL	:= fakeroot_$(HOST_FAKEROOT_VERSION).orig.$(HOST_FAKEROOT_SUFFIX)
HOST_FAKEROOT_URL	:= $(call ptx/mirror, DEB, pool/main/f/fakeroot/$(HOST_FAKEROOT_TARBALL))
HOST_FAKEROOT_SOURCE	:= $(SRCDIR)/$(HOST_FAKEROOT_TARBALL)
HOST_FAKEROOT_DIR	:= $(HOST_BUILDDIR)/$(HOST_FAKEROOT)

# Build directories for 32-bit and 64-bit x86 architectures
HOST_FAKEROOT_DIR32 := $(HOST_FAKEROOT_DIR)/build32
HOST_FAKEROOT_DIR64 := $(HOST_FAKEROOT_DIR)/build64

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

# Arguments to configure script for 32-bit builds
FAKEROOT_CONF32 := \
	$(HOST_AUTOCONF) \
	--libdir=/lib32/libfakeroot \
	--host=i686-unknown-linux-gnu \
	CFLAGS="-m32" CXXFLAGS="-m32" CPPFLAGS="-m32"

# Arguments to configure script for 64-bit builds
FAKEROOT_CONF64 := \
	$(HOST_AUTOCONF) \
	--libdir=/lib64/libfakeroot \
	--host=x86_64-unknown-linux-gnu \
	CFLAGS="-m64" CXXFLAGS="-m64" CPPFLAGS="-m64"

# Override the default ptxdist prepare stage since it builds libfakeroot
# only for the architecture of the build machine.
#
# We create two build directories named 'build32' and 'build64' within the
# extracted source directory of fakeroot package. 'configure' is run in both
# the directories separately with config required for building 32-bit and
# 64-bit libfakeroot.so libraries.
#
# We also generate a temporary Makefile outside the above build directories
# that has a generic target '%' which will capture any further make invocations
# performed by ptxdist during future build stages (like compile, install, etc)
# and then pass down the same to the makefiles inside build32 and build64
# directories.
$(STATEDIR)/host-fakeroot.prepare:
	@$(call targetinfo)
	mkdir -p $(HOST_FAKEROOT_DIR32)
	mkdir -p $(HOST_FAKEROOT_DIR64)
	cd $(HOST_FAKEROOT_DIR32) && ../configure $(FAKEROOT_CONF32)
	cd $(HOST_FAKEROOT_DIR64) && ../configure $(FAKEROOT_CONF64)
	cd $(HOST_FAKEROOT_DIR); \
		echo 'all:' > Makefile; \
		echo '%:' >> Makefile; \
		echo -e '\tmake -C $(HOST_FAKEROOT_DIR32) $$@' >> Makefile; \
		echo -e '\tmake -C $(HOST_FAKEROOT_DIR64) $$@' >> Makefile
	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/host-fakeroot.install.post:
	@$(call targetinfo)
	@$(call world/install.post, HOST_FAKEROOT)
	@sed -ie 's,FAKEROOT_SYSROOT,$(PTXCONF_SYSROOT_HOST),' \
		$(PTXCONF_SYSROOT_HOST)/bin/fakeroot
	@$(call touch)

# vim: syntax=make
