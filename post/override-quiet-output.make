
# Override targetinfo and touch macros' silent mode output to avoid using
# terminal colors and prefix each line with platform sub-type information

#
# targetinfo
#
# Print out the targetinfo line on the terminal
#
ifdef PTXDIST_QUIET
ifdef PTXDIST_FD_STDOUT
_targetinfo_opt_output := echo "[$(PLATFORM_SUBTYPE)] started : $${target}" >&$(PTXDIST_FD_STDOUT);
endif
endif

#
# touch
#
ifdef PTXDIST_QUIET
ifdef PTXDIST_FD_STDOUT
_touch_opt_output := echo "[$(PLATFORM_SUBTYPE)] finished: $${target}" >&$(PTXDIST_FD_STDOUT);
endif
endif

