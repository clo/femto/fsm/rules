
# Explicitly export make flags because Toplevel.make is unexporting it
export MAKEFLAGS MAKELEVEL MFLAGS

# Prefix the compile macro expansion with a '+' sign so that the ptxdist
# top level make would pass down the job server information to sub-make.
world/compile = +: 2>/dev/null; \
        $(call world/env, $(1)) \
        ptxd_make_world_compile

compile = +: 2>/dev/null; \
        $(call world/env, $(1)) \
        pkg_make_opt="$(2)" \
        ptxd_make_world_compile

