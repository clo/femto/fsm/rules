# -*-makefile-*-
#
# PTXdist makefile for ipsec-tools package
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_IPSEC_TOOLS) += ipsec-tools

#
# Paths and names
#
IPSEC_TOOLS_VERSION        := 0.8.2
IPSEC_TOOLS_PREFIX         := ipsec-tools
IPSEC_TOOLS                := $(IPSEC_TOOLS_PREFIX)-$(IPSEC_TOOLS_VERSION)
IPSEC_TOOLS_SUFFIX         := tar.gz
IPSEC_TOOLS_URL            := ftp://ftp.netbsd.org/pub/NetBSD/misc/ipsec-tools/0.8/$(IPSEC_TOOLS).$(IPSEC_TOOLS_SUFFIX)
IPSEC_TOOLS_SOURCE         := $(SRCDIR)/$(IPSEC_TOOLS).$(IPSEC_TOOLS_SUFFIX)
IPSEC_TOOLS_DIR            := $(BUILDDIR)/$(IPSEC_TOOLS)
IPSEC_TOOLS_ARCH           := linux

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

IPSEC_TOOLS_PATH	:= PATH=$(CROSS_PATH)
IPSEC_TOOLS_ENV 	:= $(CROSS_ENV)

#
# autoconf
#
IPSEC_TOOLS_AUTOCONF := $(CROSS_AUTOCONF_USR) --without-readline \
	--disable-security-context --without-libpam \
	--with-sysroot=$(PTXDIST_SYSROOT_TARGET) \
	--with-kernel-headers=$(PTXDIST_SYSROOT_TARGET)/usr/include

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/ipsec-tools.targetinstall:
	@$(call targetinfo)

	@$(call install_init, ipsec-tools)
	@$(call install_fixup, ipsec-tools,PACKAGE,ipsec-tools)
	@$(call install_fixup, ipsec-tools,PRIORITY,optional)
	@$(call install_fixup, ipsec-tools,VERSION,$(IPSEC_TOOLS_VERSION))
	@$(call install_fixup, ipsec-tools,SECTION,base)
	@$(call install_fixup, ipsec-tools,AUTHOR,"ipsec-tools.sourceforce.net")
	@$(call install_fixup, ipsec-tools,DEPENDS,)
	@$(call install_fixup, ipsec-tools,DESCRIPTION,"IPSec tools for Linux")

	@$(call install_copy, ipsec-tools, 0, 0, 0755, -, /usr/sbin/setkey)

	@$(call install_finish, ipsec-tools)
	@$(call touch)

