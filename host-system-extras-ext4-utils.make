# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_SYSTEM_EXTRAS_EXT4_UTILS) += host-system-extras-ext4-utils

# Paths and names
HOST_SYSTEM_EXTRAS_EXT4_UTILS_VERSION := 1.0
HOST_SYSTEM_EXTRAS_EXT4_UTILS := \
	system/extras/ext4_utils-$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_VERSION)
HOST_SYSTEM_EXTRAS_EXT4_UTILS_SRCDIR := \
	$(PTXDIST_WORKSPACE)/../opensource/system/extras/ext4_utils
HOST_SYSTEM_EXTRAS_EXT4_UTILS_URL := \
	file://$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_SRCDIR)
HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR := \
	$(HOST_BUILDDIR)/$(HOST_SYSTEM_EXTRAS_EXT4_UTILS)


# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-system-extras-ext4-utils.get: $(host-system-extras-ext4-utils.get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-system-extras-ext4-utils_extract: $(STATEDIR)/host-system-extras-ext4-utils.extract

$(STATEDIR)/host-system-extras-ext4-utils.extract: $(host-system-extras-ext4-utils.extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------
HOST_SYSTEM_EXTRAS_EXT4_UTILS_MAKE_ENV += \
	CC=$(HOSTCC) \
	AR=ar
HOST_SYSTEM_EXTRAS_EXT4_UTILS_MAKEVARS += \
	$(PARALLELMFLAGS)

EXT4_UTILS_MK = $(HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR)/Makefile
EXT4_UTILS_SRC := \
	make_ext4fs.c \
	ext4fixup.c \
	ext4_utils.c \
	allocate.c \
	contents.c \
	extent.c \
	indirect.c \
	sha1.c \
	uuid.c \
	wipe.c \
	crc16.c
EXT4_UTILS_CFLAGS := \
	$(patsubst -Wno-unused-but-set-variable,,\
	$(patsubst -Werror,,$(QCT_CFLAGS))) \
	-DHOST \
	-D__linux__ \
	-I$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_SRCDIR) \
        -I$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_SRCDIR)/../../core/include \
	-I$(PTXCONF_SYSROOT_HOST)/include

MAKE_EXT4FS_SRC := make_ext4fs_main.c $(EXT4_UTILS_SRC)
MAKE_EXT4FS_LDFLAGS := \
	-L$(PTXCONF_SYSROOT_HOST)/lib \
	-L$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR)
MAKE_EXT4FS_LDLIBS := \
	-lsparse \
	-lz \
	-lselinux

host-system-extras-ext4-utils_prepare: $(STATEDIR)/host-system-extras-ext4-utils.prepare

$(STATEDIR)/host-system-extras-ext4-utils.prepare: $(host-system-extras-ext4-utils.prepare_deps_default)
	@$(call targetinfo, $@)
	@echo 'CFLAGS += $(EXT4_UTILS_CFLAGS)' > $(EXT4_UTILS_MK)
	@echo 'LDFLAGS += $(MAKE_EXT4FS_LDFLAGS)' >> $(EXT4_UTILS_MK)
	@echo 'LDLIBS += $(MAKE_EXT4FS_LDLIBS)' >> $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo 'vpath %.c $(HOST_SYSTEM_EXTRAS_EXT4_UTILS_SRCDIR)' >> $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo 'libext4_utils_src_files := $(EXT4_UTILS_SRC)' >> $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo 'libext4_utils_src_obj := $$(libext4_utils_src_files:.c=.o)' >>  $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo 'all: libext4_utils.a make_ext4fs' >> $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo 'libext4_utils.a: $$(libext4_utils_src_obj)' >> $(EXT4_UTILS_MK)
	@echo '	$$(AR) crsP $$@ $$^' >> $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo 'make_ext4fs_src_files := $(MAKE_EXT4FS_SRC)' >> $(EXT4_UTILS_MK)
	@echo '' >> $(EXT4_UTILS_MK)
	@echo 'make_ext4fs_src_obj := $$(make_ext4fs_src_files:.c=.o)' >>  $(EXT4_UTILS_MK)
	@echo 'make_ext4fs: $$(make_ext4fs_src_obj)' >> $(EXT4_UTILS_MK)
	@echo '	$$(CC) $$(CFLAGS) $$^ -o $$@ $$(LDFLAGS) $$(LDLIBS)' >>  $(EXT4_UTILS_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-system-extras-ext4-utils_compile: $(STATEDIR)/host-system-extras-ext4-utils.compile

$(STATEDIR)/host-system-extras-ext4-utils.compile: $(host-system-extras-ext4-utils.compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_MAKE_ENV) \
		$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_PATH) $(MAKE) -C \
		$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR) \
		$(HOST_SYSTEM_EXTRAS_EXT4_UTILS_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-system-extras-ext4-utils_install: $(STATEDIR)/host-system-extras-ext4-utils.install

$(STATEDIR)/host-system-extras-ext4-utils.install: $(host-system-extras-ext4-utils.install_deps_default)
	@$(call targetinfo, $@)
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/lib
	cp -f $(HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR)/libext4_utils.a \
		$(PTXCONF_SYSROOT_HOST)/lib/
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/usr/sbin
	cp -f $(HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR)/make_ext4fs \
		$(PTXCONF_SYSROOT_HOST)/usr/sbin/
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

host-system-extras-ext4-utils_clean: $(STATEDIR)/host-system-extras-ext4-utils.clean

$(STATEDIR)/host-system-extras-ext4-utils.clean:
	rm -rf $(STATEDIR)/host-system-extras-ext4-utils.*
	rm -rf $(HOST_SYSTEM_EXTRAS_EXT4_UTILS_DIR)
