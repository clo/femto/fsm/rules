################################################################################
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#
################################################################################

#
# We provide this package
#
PACKAGES-$(PTXCONF_RSYSLOG) += rsyslog

#
# Paths and names
#
RSYSLOG_VERSION	:= 5.8.5
RSYSLOG_PREFIX	:= rsyslog
RSYSLOG		:= $(RSYSLOG_PREFIX)-$(RSYSLOG_VERSION)
RSYSLOG_SUFFIX	:= tar.gz
RSYSLOG_URL	:= http://rsyslog.com/files/download/rsyslog/$(RSYSLOG).$(RSYSLOG_SUFFIX)
RSYSLOG_DIR	:= $(BUILDDIR)/$(RSYSLOG)
RSYSLOG_SOURCE	:= $(SRCDIR)/$(RSYSLOG).$(RSYSLOG_SUFFIX)
ifeq ($(PTXCONF_RSYSLOG_CUSTOM_CONFIG),y)
RSYSLOG_ETC_DIR	:= $(PTXDIST_WORKSPACE)/projectroot/etc
RSYSLOG_CONF	:= $(RSYSLOG_ETC_DIR)/$(RSYSLOG_PREFIX).conf
RSYSLOG_INIT	:= $(RSYSLOG_ETC_DIR)/init.d/rsyslogd
RSYSLOG_ROTATE	:= $(RSYSLOG_ETC_DIR)/log_rotation_script
else
RSYSLOG_CONF	:= $(BUILDDIR)/$(RSYSLOG)/$(RSYSLOG_PREFIX).conf
endif

RSYSLOG_LIB1	:= $(BUILDDIR)/$(RSYSLOG)/runtime/.libs/lmnet.so
RSYSLOG_LIB2	:= $(BUILDDIR)/$(RSYSLOG)/plugins/imklog/.libs/imklog.so
RSYSLOG_LIB3	:= $(BUILDDIR)/$(RSYSLOG)/plugins/imuxsock/.libs/imuxsock.so
RSYSLOG_LIB4	:= $(BUILDDIR)/$(RSYSLOG)/runtime/.libs/lmnetstrms.so
RSYSLOG_LIB5	:= $(BUILDDIR)/$(RSYSLOG)/.libs/lmtcpclt.so
RSYSLOG_LIB6	:= $(BUILDDIR)/$(RSYSLOG)/plugins/immark/.libs/immark.so
RSYSLOG_LIB7	:= $(BUILDDIR)/$(RSYSLOG)/runtime/.libs/lmnsd_ptcp.so
RSYSLOG_LIB8	:= $(BUILDDIR)/$(RSYSLOG)/plugins/omrelp/.libs/omrelp.so
RSYSLOG_DSTLIB	:= /usr/lib/$(RSYSLOG_PREFIX)


# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(RSYSLOG_SOURCE):
	@$(call targetinfo)
	@$(call get, RSYSLOG)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

$(STATEDIR)/rsyslog.extract:
	@$(call targetinfo)
	@$(call clean, $(RSYSLOG_DIR))
	@$(call extract, RSYSLOG)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------
RSYSLOG_PATH	:= PATH=$(CROSS_PATH)
RSYSLOG_ENV 	:= $(CROSS_ENV)


#
# autoconf
#
RSYSLOG_AUTOCONF := $(CROSS_AUTOCONF_USR) --sbindir=/sbin --enable-relp

$(STATEDIR)/rsyslog.prepare:
	@$(call targetinfo, $@)
	cd $(RSYSLOG_DIR) && \
		$(RSYSLOG_PATH) $(RSYSLOG_ENV) \
		./configure $(RSYSLOG_AUTOCONF)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/rsyslog.compile:
	@$(call targetinfo, $@)
	$(RSYSLOG_PATH) $(RSYSLOG_ENV) $(MAKE) -C $(RSYSLOG_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/rsyslog.install:
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/rsyslog.targetinstall:
	@$(call targetinfo, $@)

	@$(call install_init, rsyslog)
	@$(call install_fixup, rsyslog,PACKAGE,rsyslog)
	@$(call install_fixup, rsyslog,PRIORITY,optional)
	@$(call install_fixup, rsyslog,VERSION,$(RSYSLOG_VERSION))
	@$(call install_fixup, rsyslog,SECTION,base)
	@$(call install_fixup, rsyslog,AUTHOR,"Rainer Gerhards")
	@$(call install_fixup, rsyslog,DEPENDS,)

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_DIR)/tools/rsyslogd, \
		/usr/bin/rsyslogd);

	@$(call install_copy, rsyslog, 0, 0, 0644, \
		$(RSYSLOG_CONF), \
		/etc/rsyslog.conf.default);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB1), \
		$(RSYSLOG_DSTLIB)/lmnet.so);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB2), \
		$(RSYSLOG_DSTLIB)/imklog.so);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB3), \
		$(RSYSLOG_DSTLIB)/imuxsock.so);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB4), \
		$(RSYSLOG_DSTLIB)/lmnetstrms.so);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB5), \
		$(RSYSLOG_DSTLIB)/lmtcpclt.so);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB6), \
		$(RSYSLOG_DSTLIB)/immark.so);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB7), \
		$(RSYSLOG_DSTLIB)/lmnsd_ptcp.so);

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_LIB8), \
		$(RSYSLOG_DSTLIB)/omrelp.so);

ifeq ($(PTXCONF_RSYSLOG_CUSTOM_CONFIG),y)
	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_INIT), \
		/etc/init.d/rsyslogd);

ifneq ($(PTXCONF_RSYSLOG__RC_D_LINK),"")
	@$(call install_link, rsyslog, \
		../init.d/rsyslogd, \
		/etc/rc.d/$(PTXCONF_RSYSLOG__RC_D_LINK))
endif

	@$(call install_copy, rsyslog, 0, 0, 0755, \
		$(RSYSLOG_ROTATE), \
		/etc/log_rotation_script);
endif

	@$(call install_finish, rsyslog)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

rsyslog_clean:
	rm -rf $(STATEDIR)/rsyslog.*
	rm -rf $(PKGDIR)/rsyslog_*
	rm -rf $(RSYSLOG_DIR)

# vim: syntax=make
