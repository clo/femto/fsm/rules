# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_SYSTEM_CORE_FASTBOOT) += host-system-core-fastboot

# Paths and names
HOST_SYSTEM_CORE_FASTBOOT_VERSION := 1.0
HOST_SYSTEM_CORE_FASTBOOT := \
	system/core/fastboot-$(HOST_SYSTEM_CORE_FASTBOOT_VERSION)
# path to directory containing source files
HOST_SYSTEM_CORE_FASTBOOT_SRCDIR := \
	$(PTXDIST_WORKSPACE)/../opensource/system/core/fastboot
HOST_SYSTEM_CORE_FASTBOOT_URL := file://$(HOST_SYSTEM_CORE_FASTBOOT_SRCDIR)
HOST_SYSTEM_CORE_FASTBOOT_DIR := $(HOST_BUILDDIR)/$(HOST_SYSTEM_CORE_FASTBOOT)


# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-system-core-fastboot.get: $(host-system-core-fastboot_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-system-core-fastboot_extract: $(STATEDIR)/host-system-core-fastboot.extract

$(STATEDIR)/host-system-core-fastboot.extract: $(host-system-core-fastboot_extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_SYSTEM_CORE_FASTBOOT_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------
HOST_SYSTEM_CORE_FASTBOOT_MAKEVARS += $(PARALLELMFLAGS)
HOST_SYSTEM_CORE_FASTBOOT_MAKE_ENV += CC=$(HOSTCC)

FASTBOOT_SRC = \
	protocol.c \
	engine.c \
	bootimg.c \
	fastboot.c \
	usb_linux.c \
	util_linux.c
FASTBOOT_MK = $(HOST_SYSTEM_CORE_FASTBOOT_DIR)/Makefile
FASTBOOT_CFLAGS := \
	$(patsubst -Wno-unused-but-set-variable,,\
		$(patsubst -Werror,,$(QCT_CFLAGS))) \
	-I$(HOST_SYSTEM_CORE_FASTBOOT_SRCDIR)/../include \
	-I$(HOST_SYSTEM_CORE_FASTBOOT_SRCDIR)/../mkbootimg \
	-I$(HOST_SYSTEM_CORE_FASTBOOT_SRCDIR)/../../extras/ext4_utils
FASTBOOT_LDFLAGS = -L$(PTXCONF_SYSROOT_HOST)/lib
FASTBOOT_LDLIBS = \
	-lzipfile \
	-lext4_utils \
	-lsparse \
	-lz \
	-lselinux

host-system-core-fastboot_prepare: $(STATEDIR)/host-system-core-fastboot.prepare

$(STATEDIR)/host-system-core-fastboot.prepare: $(host-system-core-fastboot_prepare_deps_default)
	@$(call targetinfo, $@)
	@echo 'CFLAGS += $(FASTBOOT_CFLAGS)' > $(FASTBOOT_MK)
	@echo 'LDFLAGS += $(FASTBOOT_LDFLAGS)' >> $(FASTBOOT_MK)
	@echo 'LDLIBS += $(FASTBOOT_LDLIBS)' >> $(FASTBOOT_MK)
	@echo '' >> $(FASTBOOT_MK)
	@echo 'vpath %.c $(HOST_SYSTEM_CORE_FASTBOOT_SRCDIR)' >> $(FASTBOOT_MK)
	@echo 'vpath %.c $(HOST_SYSTEM_CORE_FASTBOOT_SRCDIR)/../mkbootimg' >> $(FASTBOOT_MK)
	@echo '' >> $(FASTBOOT_MK)
	@echo 'all: fastboot' >> $(FASTBOOT_MK)
	@echo '' >> $(FASTBOOT_MK)
	@echo 'fastboot: $(FASTBOOT_SRC)' >> $(FASTBOOT_MK)
	@echo '	$$(CC) $$(CFLAGS) $$(LDFLAGS) -o $$@ $$^ $$(LDLIBS)' >> $(FASTBOOT_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-system-core-fastboot_compile: $(STATEDIR)/host-system-core-fastboot.compile

$(STATEDIR)/host-system-core-fastboot.compile: $(host-system-core-fastboot_compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_SYSTEM_CORE_FASTBOOT_MAKE_ENV) \
		$(HOST_SYSTEM_CORE_FASTBOOT_PATH) $(MAKE) -C \
		$(HOST_SYSTEM_CORE_FASTBOOT_DIR) \
		$(HOST_SYSTEM_CORE_FASTBOOT_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-system-core-fastboot_install: $(STATEDIR)/host-system-core-fastboot.install

$(STATEDIR)/host-system-core-fastboot.install: $(host-system-core-fastboot_install_deps_default)
	@$(call targetinfo, $@)
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/bin
	@cp -f $(HOST_SYSTEM_CORE_FASTBOOT_DIR)/fastboot \
		$(PTXCONF_SYSROOT_HOST)/bin/fastboot
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------
host-system-core-fastboot_clean: $(STATEDIR)/host-system-core-fastboot.clean

$(STATEDIR)/host-system-core-fastboot.clean:
	rm -rf $(STATEDIR)/host-system-core-fastboot.*
	rm -rf $(HOST_SYSTEM_CORE_FASTBOOT_DIR)
