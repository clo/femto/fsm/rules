################################################################################
#################################################################################


#
# We provide this package
#
PACKAGES-$(PTXCONF_ADB) += adb

#
# Paths and names
#
ADB_VERSION	 := 1.0.0
ADB		 := adb-$(ADB_VERSION)
ADB_SUFFIX	 := tar.gz
ADB_DIR	         := $(BUILDDIR)/$(ADB)
ADB_SRC_PATH     := $(call remove_quotes, $(PTXCONF_ADB_SRC_PATH))
ifneq "$(ADB_SRC_PATH)" ""
ADB_URL          := file://$(ADB_SRC_PATH)
ADB_SRCDIR       := $(PTXDIST_WORKSPACE)/../opensource/system/core
else
# use downloaded adb
ADB_FILENAME     := "$(call remove_quotes, $(PTXCONF_ADB_FILENAME))"
ADB_URL	         := "$(call remove_quotes, $(PTXCONF_ADB_URL))/$(ADB_FILENAME)"
ADB_SOURCE	 := $(SRCDIR)/$(ADB).$(ADB_SUFFIX)
ADB_SRCDIR       := $(ADB_DIR)
endif

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------
adb_get: $(STATEDIR)/adb.get

$(STATEDIR)/adb.get: $(adb_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)
ifeq "$(ADB_SRC_PATH)" ""
	$(shell mv -f "$(SRCDIR)/index.html$(call remove_quotes,$(ADB_FILENAME))" $(ADB_SOURCE))
endif

$(ADB_SOURCE):
	@$(call targetinfo, $@)
ifeq "$(ADB_SRC_PATH)" ""
	@$(call get, ADB)
endif

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

adb_extract: $(STATEDIR)/adb.extract

$(STATEDIR)/adb.extract: $(adb_extract_deps_default)
	@$(call targetinfo, $@)
	@$(call clean, $(ADB_DIR))
	@$(call extract, ADB)
ifeq "$(ADB_SRC_PATH)" ""
# git.source.android.com untars adb to msm7k
	@echo "using adb $(PTXCONF_ADB_COMMIT_ID)"
	if [ -d $(BUILDDIR)/core ]; then \
		mv -f $(BUILDDIR)/core $(BUILDDIR)/$(ADB); \
	fi
else 
	@echo "using local adb: $(ADB_SRC_PATH)"
endif
	@$(call touch, $@)


# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

ADB_PATH	:= PATH=$(CROSS_PATH)
ADB_ENV 	:= $(CROSS_ENV)

#
#
# Build in the platform-XXXX/build-<host|target> directory
 ADB_MAKEVARS += -C $(ADB_DIR)

# Location of source directory used by vpath
 ADB_MAKEVARS += OBJDIR=$(ADB_DIR)
 ADB_MAKEVARS += SRCDIR=$(ADB_DIR)
 ADB_MAKEVARS += $(PARALLELMFLAGS)
 ADB_MAKEVARS += CROSS_COMPILE=arm-none-linux-gnueabi-

$(STATEDIR)/adb.prepare:
	@$(call targetinfo)
	@$(call touch)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/adb.compile:
	@$(call targetinfo)
	$(ADB_ENV) $(ADB_PATH) $(MAKE) $(ADB_MAKEVARS) -f $(ADB_SRCDIR)/libcutils/libcutils.mk
	$(ADB_ENV) $(ADB_PATH) $(MAKE) $(ADB_MAKEVARS) -f $(ADB_SRCDIR)/adb/adb.mk
	$(ADB_ENV) $(ADB_PATH) $(MAKE) $(ADB_MAKEVARS) -f $(ADB_SRCDIR)/toolbox/toolbox.mk
	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/adb.install:
	@$(call targetinfo)
	@$(call touch)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/adb.targetinstall:
	@$(call targetinfo)

	@$(call install_init, adb)
	@$(call install_fixup, adb,PACKAGE,adb)
	@$(call install_fixup, adb,PRIORITY,optional)
	@$(call install_fixup, adb,VERSION,$(ADB_VERSION))
	@$(call install_fixup, adb,SECTION,base)
	@$(call install_fixup, adb,DEPENDS,)
	@$(call install_fixup, adb,DESCRIPTION,missing)

	@$(call install_copy, adb, 0, 0, 0755, \
	$(PTXDIST_WORKSPACE)/projectroot/etc/udev/10-adb-daemon.rules, \
 	$(PTXCONF_QCOM_BIN_INSTALL_PATH)/../../../lib/udev/rules.d/10-adb-daemon.rules)
	@$(call install_copy, adb, 0, 0, 0755, $(ADB_DIR)/adbd, $(PTXCONF_QCOM_BIN_INSTALL_PATH)/adbd)
	@$(call install_copy, adb, 0, 0, 0755, $(ADB_DIR)/toolbox/toolbox, $(PTXCONF_QCOM_BIN_INSTALL_PATH)/toolbox)
	@$(call install_link, adb, toolbox, $(PTXCONF_QCOM_BIN_INSTALL_PATH)/ls)
	@$(call install_finish, adb)

	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

adb_clean:
	rm -rf $(STATEDIR)/adb.*
	rm -rf $(PKGDIR)/adb_*
	rm -rf $(ADB_DIR)

# vim: syntax=make
