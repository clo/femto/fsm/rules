# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_SYSTEM_CORE_LIBSPARSE) += host-system-core-libsparse

# Paths and names
HOST_SYSTEM_CORE_LIBSPARSE_VERSION := 1.0
HOST_SYSTEM_CORE_LIBSPARSE         := system/core/libsparse-$(HOST_SYSTEM_CORE_LIBSPARSE_VERSION)
# path to directory containing source files
HOST_SYSTEM_CORE_LIBSPARSE_SRCDIR  := $(PTXDIST_WORKSPACE)/../opensource/system/core/libsparse
HOST_SYSTEM_CORE_LIBSPARSE_URL     := file://$(HOST_SYSTEM_CORE_LIBSPARSE_SRCDIR)
HOST_SYSTEM_CORE_LIBSPARSE_DIR     := $(HOST_BUILDDIR)/$(HOST_SYSTEM_CORE_LIBSPARSE)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-system-core-libsparse.get: $(host-system-core-libsparse_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-system-core-libsparse_extract: $(STATEDIR)/host-system-core-libsparse.extract

$(STATEDIR)/host-system-core-libsparse.extract: $(host-system-core-libsparse_extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_SYSTEM_CORE_LIBSPARSE_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

# Location of source directory used by vpath
HOST_SYSTEM_CORE_LIBSPARSE_MAKEVARS += $(PARALLELMFLAGS)
HOST_SYSTEM_CORE_LIBSPARSE_MAKE_ENV += \
	CC=$(HOSTCC) \
	AR=ar

LIBSPARSE_SRC = \
	backed_block.c \
	output_file.c \
	sparse.c \
	sparse_crc32.c \
	sparse_err.c \
	sparse_read.c
LIBSPARSE_CFLAGS := \
	$(patsubst -Wno-unused-but-set-variable,,\
		$(patsubst -Werror,,$(QCT_CFLAGS))) \
	-I$(HOST_SYSTEM_CORE_LIBSPARSE_SRCDIR)/include
LIBSPARSE_MK = $(HOST_SYSTEM_CORE_LIBSPARSE_DIR)/Makefile
LIBSPARSE_LDFLAGS = -L$(PTXCONF_SYSROOT_HOST)/lib
LIBSPARSE_LDLIBS = -lz

host-system-core-libsparse_prepare: $(STATEDIR)/host-system-core-libsparse.prepare

$(STATEDIR)/host-system-core-libsparse.prepare: $(host-system-core-libsparse_prepare_deps_default)
	@$(call targetinfo, $@)
	@echo 'CFLAGS += $(LIBSPARSE_CFLAGS)' > $(LIBSPARSE_MK)
	@echo 'LDFLAGS += $(LIBSPARSE_LDFLAGS)' >> $(LIBSPARSE_MK)
	@echo 'LDLIBS += $(LIBSPARSE_LDLIBS)' >> $(LIBSPARSE_MK)
	@echo '' >> $(LIBSPARSE_MK)
	@echo 'vpath %.c $(HOST_SYSTEM_CORE_LIBSPARSE_SRCDIR)' >> $(LIBSPARSE_MK)
	@echo '' >> $(LIBSPARSE_MK)
	@echo 'libsparse_src_files := $(LIBSPARSE_SRC)' >>  $(LIBSPARSE_MK)
	@echo 'libsparse_src_obj := $$(libsparse_src_files:.c=.o)' >>  $(LIBSPARSE_MK)
	@echo 'all: libsparse.a' >> $(LIBSPARSE_MK)
	@echo '' >> $(LIBSPARSE_MK)
	@echo 'libsparse.a: $$(libsparse_src_obj)' >> $(LIBSPARSE_MK)
	@echo '	$$(AR) crsP $$@ $$^' >> $(LIBSPARSE_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-system-core-libsparse_compile: $(STATEDIR)/host-system-core-libsparse.compile

$(STATEDIR)/host-system-core-libsparse.compile: $(host-system-core-libsparse_compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_SYSTEM_CORE_LIBSPARSE_MAKE_ENV) \
		$(HOST_SYSTEM_CORE_LIBSPARSE_PATH) $(MAKE) -C \
		$(HOST_SYSTEM_CORE_LIBSPARSE_DIR) \
		$(HOST_SYSTEM_CORE_LIBSPARSE_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-system-core-libsparse_install: $(STATEDIR)/host-system-core-libsparse.install

$(STATEDIR)/host-system-core-libsparse.install: $(host-system-core-libsparse_install_deps_default)
	@$(call targetinfo, $@)
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/lib
	@cp -f $(HOST_SYSTEM_CORE_LIBSPARSE_DIR)/libsparse.a \
		$(PTXCONF_SYSROOT_HOST)/lib/
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/include
	@cp -rf $(HOST_SYSTEM_CORE_LIBSPARSE_SRCDIR)/include/sparse \
		$(PTXCONF_SYSROOT_HOST)/include/
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------
host-system-core-libsparse_clean: $(STATEDIR)/host-system-core-libsparse.clean

$(STATEDIR)/host-system-core-libsparse.clean:
	rm -rf $(STATEDIR)/host-system-core-libsparse.*
	rm -rf $(HOST_SYSTEM_CORE_LIBSPARSE_DIR)
