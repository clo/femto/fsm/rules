# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_SYSTEM_CORE_LIBMINCRYPT) += host-system-core-libmincrypt

# Paths and names
HOST_SYSTEM_CORE_LIBMINCRYPT_VERSION := 1.0
HOST_SYSTEM_CORE_LIBMINCRYPT         := system/core/libmincrypt-$(HOST_SYSTEM_CORE_LIBMINCRYPT_VERSION)
# path to directory containing source files
HOST_SYSTEM_CORE_LIBMINCRYPT_SRCDIR  := $(PTXDIST_WORKSPACE)/../opensource/system/core/libmincrypt
HOST_SYSTEM_CORE_LIBMINCRYPT_URL     := file://$(HOST_SYSTEM_CORE_LIBMINCRYPT_SRCDIR)
HOST_SYSTEM_CORE_LIBMINCRYPT_DIR     := $(HOST_BUILDDIR)/$(HOST_SYSTEM_CORE_LIBMINCRYPT)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-system-core-libmincrypt.get: $(host-system-core-libmincrypt_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-system-core-libmincrypt_extract: $(STATEDIR)/host-system-core-libmincrypt.extract

$(STATEDIR)/host-system-core-libmincrypt.extract: $(host-system-core-libmincrypt_extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_SYSTEM_CORE_LIBMINCRYPT_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

# Location of source directory used by vpath
HOST_SYSTEM_CORE_LIBMINCRYPT_MAKEVARS += $(PARALLELMFLAGS)
HOST_SYSTEM_CORE_LIBMINCRYPT_MAKE_ENV += \
	CC=$(HOSTCC) \
	AR=ar

LIBMINCRYPT_SRC = \
	rsa.c \
	sha.c \
	sha256.c
LIBMINCRYPT_CFLAGS := \
	$(patsubst -Wno-unused-but-set-variable,,\
		$(patsubst -Werror,,$(QCT_CFLAGS))) \
	-I$(HOST_SYSTEM_CORE_LIBMINCRYPT_SRCDIR)/../include
LIBMINCRYPT_MK = $(HOST_SYSTEM_CORE_LIBMINCRYPT_DIR)/Makefile

host-system-core-libmincrypt_prepare: $(STATEDIR)/host-system-core-libmincrypt.prepare

$(STATEDIR)/host-system-core-libmincrypt.prepare: $(host-system-core-libmincrypt_prepare_deps_default)
	@$(call targetinfo, $@)
	@echo 'CFLAGS += $(LIBMINCRYPT_CFLAGS)' > $(LIBMINCRYPT_MK)
	@echo '' >> $(LIBMINCRYPT_MK)
	@echo 'vpath %.c $(HOST_SYSTEM_CORE_LIBMINCRYPT_SRCDIR)' >> $(LIBMINCRYPT_MK)
	@echo '' >> $(LIBMINCRYPT_MK)
	@echo 'libmincrypt_src_files := $(LIBMINCRYPT_SRC)' >> $(LIBMINCRYPT_MK)
	@echo 'libmincrypt_src_obj := $$(libmincrypt_src_files:.c=.o)' >> $(LIBMINCRYPT_MK)
	@echo '' >> $(LIBMINCRYPT_MK)
	@echo 'all: libmincrypt.a' >> $(LIBMINCRYPT_MK)
	@echo '' >> $(LIBMINCRYPT_MK)
	@echo 'libmincrypt.a: $$(libmincrypt_src_obj)' >> $(LIBMINCRYPT_MK)
	@echo '	$$(AR) crsP $$@ $$^' >> $(LIBMINCRYPT_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-system-core-libmincrypt_compile: $(STATEDIR)/host-system-core-libmincrypt.compile

$(STATEDIR)/host-system-core-libmincrypt.compile: $(host-system-core-libmincrypt_compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_SYSTEM_CORE_LIBMINCRYPT_MAKE_ENV) \
		$(HOST_SYSTEM_CORE_LIBMINCRYPT_PATH) $(MAKE) -C \
		$(HOST_SYSTEM_CORE_LIBMINCRYPT_DIR) \
		$(HOST_SYSTEM_CORE_LIBMINCRYPT_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-system-core-libmincrypt_install: $(STATEDIR)/host-system-core-libmincrypt.install

$(STATEDIR)/host-system-core-libmincrypt.install: $(host-system-core-libmincrypt_install_deps_default)
	@$(call targetinfo, $@)
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/lib
	@cp -f $(HOST_SYSTEM_CORE_LIBMINCRYPT_DIR)/libmincrypt.a \
		$(PTXCONF_SYSROOT_HOST)/lib/
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------
host-system-core-libmincrypt_clean: $(STATEDIR)/host-system-core-libmincrypt.clean

$(STATEDIR)/host-system-core-libmincrypt.clean:
	rm -rf $(STATEDIR)/host-system-core-libmincrypt.*
	rm -rf $(HOST_SYSTEM_CORE_LIBMINCRYPT_DIR)
