# -*-makefile-*-
# $Id: template 6655 2007-01-02 12:55:21Z rsc $
#
# Copyright (C) 2005 by Robert Schwebel
#               2007 by Marc Kleine-Budde <mkl@pengutronix.de>
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_BLUEZ_UTILS) += bluez-utils

#
# Paths and names
#
BLUEZ_UTILS_VERSION	:= 3.32
BLUEZ_UTILS		:= bluez-utils-$(BLUEZ_UTILS_VERSION)
BLUEZ_UTILS_SUFFIX	:= tar.gz
BLUEZ_UTILS_URL		:= http://bluez.sourceforge.net/download/$(BLUEZ_UTILS).$(BLUEZ_UTILS_SUFFIX)
BLUEZ_UTILS_SOURCE	:= $(SRCDIR)/$(BLUEZ_UTILS).$(BLUEZ_UTILS_SUFFIX)
BLUEZ_UTILS_DIR		:= $(BUILDDIR)/$(BLUEZ_UTILS)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

bluez-utils_get: $(STATEDIR)/bluez-utils.get

$(STATEDIR)/bluez-utils.get: $(bluez-utils_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

$(BLUEZ_UTILS_SOURCE):
	@$(call targetinfo, $@)
	@$(call get, BLUEZ_UTILS)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

bluez-utils_extract: $(STATEDIR)/bluez-utils.extract

$(STATEDIR)/bluez-utils.extract: $(bluez-utils_extract_deps_default)
	@$(call targetinfo, $@)
	@$(call clean, $(BLUEZ_UTILS_DIR))
	@$(call extract, BLUEZ_UTILS)
	@$(call patchin, BLUEZ_UTILS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

bluez-utils_prepare: $(STATEDIR)/bluez-utils.prepare

BLUEZ_UTILS_PATH	:= PATH=$(CROSS_PATH)
BLUEZ_UTILS_ENV 	:= $(CROSS_ENV)

#
# autoconf
#
BLUEZ_UTILS_AUTOCONF := \
	$(CROSS_AUTOCONF_USR) \
	--enable-alsa \
	--enable-audio \
	--without-fuse \
	--without-openobex \
	--without-usb \
	--enable-test

$(STATEDIR)/bluez-utils.prepare: $(bluez-utils_prepare_deps_default)
	@$(call targetinfo, $@)
	@$(call clean, $(BLUEZ_UTILS_DIR)/config.cache)
	cd $(BLUEZ_UTILS_DIR) && \
		$(BLUEZ_UTILS_PATH) $(BLUEZ_UTILS_ENV) \
		./configure $(BLUEZ_UTILS_AUTOCONF)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

bluez-utils_compile: $(STATEDIR)/bluez-utils.compile

$(STATEDIR)/bluez-utils.compile: $(bluez-utils_compile_deps_default)
	@$(call targetinfo, $@)
	cd $(BLUEZ_UTILS_DIR) && $(BLUEZ_UTILS_PATH) $(MAKE) $(PARALLELMFLAGS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

bluez-utils_install: $(STATEDIR)/bluez-utils.install

$(STATEDIR)/bluez-utils.install: $(bluez-utils_install_deps_default)
	@$(call targetinfo, $@)
	@$(call install, BLUEZ_UTILS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

bluez-utils_targetinstall: $(STATEDIR)/bluez-utils.targetinstall

$(STATEDIR)/bluez-utils.targetinstall: $(bluez-utils_targetinstall_deps_default)
	@$(call targetinfo, $@)

	@$(call install_init, bluez-utils)
	@$(call install_fixup, bluez-utils,PACKAGE,bluez-utils)
	@$(call install_fixup, bluez-utils,PRIORITY,optional)
	@$(call install_fixup, bluez-utils,VERSION,$(BLUEZ_UTILS_VERSION))
	@$(call install_fixup, bluez-utils,SECTION,base)
	@$(call install_fixup, bluez-utils,AUTHOR,"Robert Schwebel <r.schwebel\@pengutronix.de>")
	@$(call install_fixup, bluez-utils,DEPENDS,)
	@$(call install_fixup, bluez-utils,DESCRIPTION,missing)

	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/tools/hcitool, /usr/bin/hcitool)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/tools/hciconfig, /usr/bin/hciconfig)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/tools/l2ping, /usr/bin/l2ping)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/test/rctest, /usr/bin/rctest)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/test/l2test, /usr/bin/l2test)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/rfcomm/rfcomm, /usr/bin/rfcomm)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/tools/sdptool, /usr/bin/sdptool)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/tools/hciattach, /usr/bin/hciattach)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/hcid/hcid, /usr/sbin/hcid)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/daemon/passkey-agent, /usr/bin/passkey-agent)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/hcid/hcid.conf, /etc/bluetooth/hcid.conf,n)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/audio/audio.conf, /etc/bluetooth/audio.conf,n)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/rfcomm/rfcomm.conf, /etc/bluetooth/rfcomm.conf,n)
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/audio/.libs/libasound_module_pcm_bluetooth.so, /usr/lib/alsa-lib/libasound_module_pcm_bluetooth.so)	

	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/plugins/libaudio.so, /usr/lib/bluetooth/plugins/libaudio.so)
	@$(call install_copy, bluez-utils, 0, 0, 0755, /var/lib/bluetooth)	
	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/daemon/bluetooth.conf, /etc/dbus-1/system.d/bluetooth.conf,n)
	
	# FIXME: wait for patch from Sandro Noel
#	@$(call install_copy, bluez-utils, 0, 0, 0755, $(BLUEZ_UTILS_DIR)/foobar, /dev/null)

	@$(call install_finish, bluez-utils)

	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

bluez-utils_clean:
	rm -rf $(STATEDIR)/bluez-utils.*
	rm -rf $(PKGDIR)/bluez-utils_*
	rm -rf $(BLUEZ_UTILS_DIR)

# vim: syntax=make
