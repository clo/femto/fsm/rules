#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.

#
# We provide this package
#
PACKAGES-$(PTXCONF_LIBXMLRPC) += libxmlrpc

#
# Paths and names
#
LIBXMLRPC_VERSION	:= 1.36.02
LIBXMLRPC_SVNREVISION	:= 2552
LIBXMLRPC_LIBSUFFIX	:= 3
LIBXMLRPC_PREFIX	:= xmlrpc-c-advanced
LIBXMLRPC_SUFFIX	:= tar.gz
LIBXMLRPC		:= $(LIBXMLRPC_PREFIX)-$(LIBXMLRPC_VERSION).$(LIBXMLRPC_SUFFIX)
LIBXMLRPC_URL		:= svn://svn.code.sf.net/p/xmlrpc-c/code/advanced;rev=$(LIBXMLRPC_SVNREVISION)

LIBXMLRPC_SOURCE	:= $(SRCDIR)/$(LIBXMLRPC)
LIBXMLRPC_DIR		:= $(BUILDDIR)/$(LIBXMLRPC_PREFIX)/$(LIBXMLRPC_VERSION)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

libxmlrpc_get: $(STATEDIR)/libxmlrpc.get

$(STATEDIR)/libxmlrpc.get: $(libxmlrpc_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

$(LIBXMLRPC_SOURCE):
	@$(call targetinfo)
	@$(call get, LIBXMLRPC)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

libxmlrpc_extract: $(STATEDIR)/libxmlrpc.extract

$(STATEDIR)/libxmlrpc.extract:
	@$(call targetinfo)
	@$(call clean, $(LIBXMLRPC_DIR))
	@$(call extract, LIBXMLRPC, $(BUILDDIR)/$(LIBXMLRPC_PREFIX) )
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

libxmlrpc_prepare: $(STATEDIR)/libxmlrpc.prepare

LIBXMLRPC_PATH	   := PATH=$(CROSS_PATH)
LIBXMLRPC_ENV 	   := $(CROSS_ENV)
LIBXMLRPC_MAKEVARS += -I $(PTXCONF_SYSROOT_TARGET)/usr/include/libxml2
LIBXMLRPC_MAKEVARS += -j1

# autoconf

LIBXMLRPC_AUTOCONF := $(CROSS_AUTOCONF_USR)

$(STATEDIR)/libxmlrpc.prepare:
	@$(call targetinfo, $@)
	@$(call clean, $(LIBXMLRPC_DIR)/config.cache)
	cd $(LIBXMLRPC_DIR) && \
		./configure --disable-wininet-client --disable-curl-client \
		--disable-cgi-server --disable-libwww-client \
		--enable-libxml2-backend $(LIBXMLRPC_AUTOCONF) $(LIBXMLRPC_PATH)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

libxmlrpc_compile: $(STATEDIR)/libxmlrpc.compile

$(STATEDIR)/libxmlrpc.compile:
	@$(call targetinfo, $@)
	cd $(LIBXMLRPC_DIR) && $(LIBXMLRPC_ENV) $(LIBXMLRPC_PATH) $(MAKE) $(LIBXMLRPC_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

libxmlrpc_install: $(STATEDIR)/libxmlrpc.install

$(STATEDIR)/libxmlrpc.install:
	@$(call targetinfo, $@)
	@$(call install, LIBXMLRPC)
	@$(call touch, $@)


# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

libxmlrpc_targetinstall: $(STATEDIR)/libxmlrpc.targetinstall

$(STATEDIR)/libxmlrpc.targetinstall:
	@$(call targetinfo)

	@$(call install_init, libxmlrpc)
	@$(call install_fixup, libxmlrpc,PACKAGE,libxmlrpc)
	@$(call install_fixup, libxmlrpc,PRIORITY,optional)
	@$(call install_fixup, libxmlrpc,VERSION,$(LIBXMLRPC_VERSION))
	@$(call install_fixup, libxmlrpc,SECTION,base)
	@$(call install_fixup, libxmlrpc,AUTHOR,missing)
	@$(call install_fixup, libxmlrpc,DEPENDS,)
	@$(call install_fixup, libxmlrpc,DESCRIPTION,missing)

	@$(call install_copy, libxmlrpc, 0, 0, 0644, $(LIBXMLRPC_DIR)/src/libxmlrpc.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc.so, y)
	@$(call install_link, libxmlrpc, libxmlrpc.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc.so.$(LIBXMLRPC_LIBSUFFIX))

	@$(call install_copy, libxmlrpc, 0, 0, 0644, $(LIBXMLRPC_DIR)/src/libxmlrpc_server_abyss.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_server_abyss.so, y)
	@$(call install_link, libxmlrpc, libxmlrpc_server_abyss.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_server_abyss.so.$(LIBXMLRPC_LIBSUFFIX))

	@$(call install_copy, libxmlrpc, 0, 0, 0644, $(LIBXMLRPC_DIR)/lib/libutil/libxmlrpc_util.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_util.so, y)
	@$(call install_link, libxmlrpc, libxmlrpc_util.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_util.so.$(LIBXMLRPC_LIBSUFFIX))

	@$(call install_copy, libxmlrpc, 0, 0, 0644, $(LIBXMLRPC_DIR)/src/libxmlrpc_server.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_server.so, y)
	@$(call install_link, libxmlrpc, libxmlrpc_server.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_server.so.$(LIBXMLRPC_LIBSUFFIX))

	@$(call install_copy, libxmlrpc, 0, 0, 0644, $(LIBXMLRPC_DIR)/lib/abyss/src/libxmlrpc_abyss.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_abyss.so, y)
	@$(call install_link, libxmlrpc, libxmlrpc_abyss.so, $(PTXCONF_QCOM_LIB_INSTALL_PATH)/libxmlrpc_abyss.so.$(LIBXMLRPC_LIBSUFFIX))

	@$(call install_finish, libxmlrpc)

	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

libxmlrpc_clean:
	rm -rf $(STATEDIR)/libxmlrpc.*
	rm -rf $(PKGDIR)/libxmlrpc*.*
	rm -rf $(LIBXMLRPC_DIR)

# vim: syntax=make
