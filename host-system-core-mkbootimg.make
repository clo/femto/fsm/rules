# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_SYSTEM_CORE_MKBOOTIMG) += host-system-core-mkbootimg

# Paths and names
HOST_SYSTEM_CORE_MKBOOTIMG_VERSION := 1.0
HOST_SYSTEM_CORE_MKBOOTIMG := \
	system/core/mkbootimg-$(HOST_SYSTEM_CORE_MKBOOTIMG_VERSION)
# path to directory containing source files
HOST_SYSTEM_CORE_MKBOOTIMG_SRCDIR := \
	$(PTXDIST_WORKSPACE)/../opensource/system/core/mkbootimg
HOST_SYSTEM_CORE_MKBOOTIMG_URL := file://$(HOST_SYSTEM_CORE_MKBOOTIMG_SRCDIR)
# location in project/<platform>/build-host/core-0.01 symlink to CORE_URL
HOST_SYSTEM_CORE_MKBOOTIMG_DIR := $(HOST_BUILDDIR)/$(HOST_SYSTEM_CORE_MKBOOTIMG)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-system-core-mkbootimg.get: $(host-system-core-mkbootimg_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-system-core-mkbootimg_extract: $(STATEDIR)/host-system-core-mkbootimg.extract

$(STATEDIR)/host-system-core-mkbootimg.extract: $(host-system-core-mkbootimg_extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_SYSTEM_CORE_MKBOOTIMG_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------
HOST_SYSTEM_CORE_MKBOOTIMG_MAKEVARS += $(PARALLELMFLAGS)
HOST_SYSTEM_CORE_MKBOOTIMG_MAKE_ENV += CC=$(HOSTCC)

MKBOOTIMG_SRC = mkbootimg.c
MKBOOTIMG_MK = $(HOST_SYSTEM_CORE_MKBOOTIMG_DIR)/Makefile
MKBOOTIMG_CFLAGS := \
	$(patsubst -Wno-unused-but-set-variable,,\
		$(patsubst -Werror,,$(QCT_CFLAGS))) \
	-I$(HOST_SYSTEM_CORE_MKBOOTIMG_SRCDIR)/../include
MKBOOTIMG_LDFLAGS = -L$(PTXCONF_SYSROOT_HOST)/lib
MKBOOTIMG_LDLIBS = -lmincrypt

host-system-core-mkbootimg_prepare: $(STATEDIR)/host-system-core-mkbootimg.prepare

$(STATEDIR)/host-system-core-mkbootimg.prepare: $(host-system-core-mkbootimg_prepare_deps_default)
	@$(call targetinfo, $@)
	@echo 'CFLAGS += $(MKBOOTIMG_CFLAGS)' > $(MKBOOTIMG_MK)
	@echo 'LDFLAGS = $(MKBOOTIMG_LDFLAGS)' >> $(MKBOOTIMG_MK)
	@echo 'LDLIBS = $(MKBOOTIMG_LDLIBS)' >> $(MKBOOTIMG_MK)
	@echo '' >> $(MKBOOTIMG_MK)
	@echo 'vpath %.c $(HOST_SYSTEM_CORE_MKBOOTIMG_SRCDIR)' >> $(MKBOOTIMG_MK)
	@echo '' >> $(MKBOOTIMG_MK)
	@echo 'all: mkbootimg' >> $(MKBOOTIMG_MK)
	@echo '' >> $(MKBOOTIMG_MK)
	@echo 'mkbootimg: $(MKBOOTIMG_SRC)' >> $(MKBOOTIMG_MK)
	@echo '	$$(CC) $$(CFLAGS) $$(LDFLAGS) -o $$@ $$^ $$(LDLIBS)' >> $(MKBOOTIMG_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-system-core-mkbootimg_compile: $(STATEDIR)/host-system-core-mkbootimg.compile

$(STATEDIR)/host-system-core-mkbootimg.compile: $(host-system-core-mkbootimg_compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_SYSTEM_CORE_MKBOOTIMG_MAKE_ENV) \
		$(HOST_SYSTEM_CORE_MKBOOTIMG_PATH) $(MAKE) -C \
		$(HOST_SYSTEM_CORE_MKBOOTIMG_DIR) \
		$(HOST_SYSTEM_CORE_MKBOOTIMG_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-system-core-mkbootimg_install: $(STATEDIR)/host-system-core-mkbootimg.install

$(STATEDIR)/host-system-core-mkbootimg.install: $(host-system-core-mkbootimg_install_deps_default)
	@$(call targetinfo, $@)
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/bin
	@cp -f $(HOST_SYSTEM_CORE_MKBOOTIMG_DIR)/mkbootimg $(PTXCONF_SYSROOT_HOST)/bin/mkbootimg
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

host-system-core-mkbootimg_clean: $(STATEDIR)/host-system-core-mkbootimg.clean

$(STATEDIR)/host-system-core-mkbootimg.clean:
	rm -rf $(STATEDIR)/host-system-core-mkbootimg.*
	rm -rf $(HOST_SYSTEM_CORE_MKBOOTIMG_DIR)
