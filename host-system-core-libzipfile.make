# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_SYSTEM_CORE_LIBZIPFILE) += host-system-core-libzipfile

# Paths and names
HOST_SYSTEM_CORE_LIBZIPFILE_VERSION := 1.0
HOST_SYSTEM_CORE_LIBZIPFILE         := system/core/libzipfile-$(HOST_SYSTEM_CORE_LIBZIPFILE_VERSION)
# path to directory containing source files
HOST_SYSTEM_CORE_LIBZIPFILE_SRCDIR  := $(PTXDIST_WORKSPACE)/../opensource/system/core/libzipfile
HOST_SYSTEM_CORE_LIBZIPFILE_URL     := file://$(HOST_SYSTEM_CORE_LIBZIPFILE_SRCDIR)
HOST_SYSTEM_CORE_LIBZIPFILE_DIR     := $(HOST_BUILDDIR)/$(HOST_SYSTEM_CORE_LIBZIPFILE)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-system-core-libzipfile.get: $(host-system-core-libzipfile_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-system-core-libzipfile_extract: $(STATEDIR)/host-system-core-libzipfile.extract

$(STATEDIR)/host-system-core-libzipfile.extract: $(host-system-core-libzipfile_extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_SYSTEM_CORE_LIBZIPFILE_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

# Location of source directory used by vpath
HOST_SYSTEM_CORE_LIBZIPFILE_MAKEVARS += $(PARALLELMFLAGS)
HOST_SYSTEM_CORE_LIBZIPFILE_MAKE_ENV += \
	CC=$(HOSTCC) \
	AR=ar

LIBZIPFILE_SRC = \
	centraldir.c \
	zipfile.c
LIBZIPFILE_CFLAGS := \
	$(patsubst -Wno-unused-but-set-variable,,\
		$(patsubst -Werror,,$(QCT_CFLAGS))) \
	-I$(HOST_SYSTEM_CORE_LIBZIPFILE_SRCDIR)/../include
LIBZIPFILE_LDFLAGS := -L$(PTXCONF_SYSROOT_HOST)/lib
LIBZIPFILE_LDLIBS := -lz
LIBZIPFILE_MK = $(HOST_SYSTEM_CORE_LIBZIPFILE_DIR)/Makefile

host-system-core-libzipfile_prepare: $(STATEDIR)/host-system-core-libzipfile.prepare

$(STATEDIR)/host-system-core-libzipfile.prepare: $(host-system-core-libzipfile_prepare_deps_default)
	@$(call targetinfo, $@)
	@echo 'CFLAGS += $(LIBZIPFILE_CFLAGS)' > $(LIBZIPFILE_MK)
	@echo 'LDFLAGS += $(LIBZIPFILE_LDFLAGS)' >> $(LIBZIPFILE_MK)
	@echo 'LDLIBS += $(LIBZIPFILE_LDLIBS)' >> $(LIBZIPFILE_MK)
	@echo '' >> $(LIBZIPFILE_MK)
	@echo 'vpath %.c $(HOST_SYSTEM_CORE_LIBZIPFILE_SRCDIR)' >> $(LIBZIPFILE_MK)
	@echo '' >> $(LIBZIPFILE_MK)
	@echo 'libzipfile_src_files := $(LIBZIPFILE_SRC)' >> $(LIBZIPFILE_MK)
	@echo 'libzipfile_src_obj := $$(libzipfile_src_files:.c=.o)' >> $(LIBZIPFILE_MK)
	@echo 'all: libzipfile.a' >> $(LIBZIPFILE_MK)
	@echo '' >> $(LIBZIPFILE_MK)
	@echo 'libzipfile.a: $$(libzipfile_src_obj)' >> $(LIBZIPFILE_MK)
	@echo '	$$(AR) crsP $$@ $$^' >> $(LIBZIPFILE_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-system-core-libzipfile_compile: $(STATEDIR)/host-system-core-libzipfile.compile

$(STATEDIR)/host-system-core-libzipfile.compile: $(host-system-core-libzipfile_compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_SYSTEM_CORE_LIBZIPFILE_MAKE_ENV) \
		$(HOST_SYSTEM_CORE_LIBZIPFILE_PATH) $(MAKE) -C \
		$(HOST_SYSTEM_CORE_LIBZIPFILE_DIR) \
		$(HOST_SYSTEM_CORE_LIBZIPFILE_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-system-core-libzipfile_install: $(STATEDIR)/host-system-core-libzipfile.install

$(STATEDIR)/host-system-core-libzipfile.install: $(host-system-core-libzipfile_install_deps_default)
	@$(call targetinfo, $@)
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/lib
	@cp -f $(HOST_SYSTEM_CORE_LIBZIPFILE_DIR)/libzipfile.a \
		$(PTXCONF_SYSROOT_HOST)/lib/
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/include
	@cp -rf $(HOST_SYSTEM_CORE_LIBZIPFILE_SRCDIR)/../include/zipfile \
		$(PTXCONF_SYSROOT_HOST)/include/
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------
host-system-core-libzipfile_clean: $(STATEDIR)/host-system-core-libzipfile.clean

$(STATEDIR)/host-system-core-libzipfile.clean:
	rm -rf $(STATEDIR)/host-system-core-libzipfile.*
	rm -rf $(HOST_SYSTEM_CORE_LIBZIPFILE_DIR)
