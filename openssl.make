# -*-makefile-*-
#
# Copyright (C) 2002 by Jochen Striepe for Pengutronix e.K., Hildesheim, Germany
#               2003-2008 by Pengutronix e.K., Hildesheim, Germany
#		2009 by Marc Kleine-Budde <mkl@pengutronix.de>
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_OPENSSL) += openssl

#
# Paths and names
#
OPENSSL_VERSION	:= 1.0.1h
OPENSSL_MD5	:= 8d6d684a9430d5cc98a62a5d8fbda8cf
OPENSSL		:= openssl-$(OPENSSL_VERSION)
OPENSSL_SUFFIX	:= tar.gz
OPENSSL_URL	:= http://www.openssl.org/source/$(OPENSSL).$(OPENSSL_SUFFIX)
OPENSSL_SOURCE	:= $(SRCDIR)/$(OPENSSL).$(OPENSSL_SUFFIX)
OPENSSL_DIR	:= $(BUILDDIR)/$(OPENSSL)
OPENSSL_LICENSE	:= openssl

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

OPENSSL_CONF_ENV 	:= $(CROSS_ENV)
OPENSSL_MAKE_VARS	:= -j1

OPENSSL_ARCH-$(PTXCONF_ARCH_X86_I386)   += linux-i386
OPENSSL_ARCH-$(PTXCONF_ARCH_X86_I486)   += linux-i386-i486
OPENSSL_ARCH-$(PTXCONF_ARCH_X86_I586)   += linux-i386-i586
OPENSSL_ARCH-$(PTXCONF_ARCH_X86_I686)   += linux-i386-i686/cmov
OPENSSL_ARCH-$(PTXCONF_ARCH_X86_P2) += linux-i386-i686/cmov
OPENSSL_ARCH-$(PTXCONF_ARCH_X86_P3M)    += linux-i386-i686/cmov
OPENSSL_ARCH-$(PTXCONF_ARCH_M68K)   += linux-m68k
OPENSSL_ARCH-$(PTXCONF_ARCH_PPC)    += linux-powerpc
OPENSSL_ARCH-$(PTXCONF_ARCH_SPARC)  += linux-sparc

ifdef PTXCONF_ENDIAN_LITTLE
OPENSSL_ARCH-$(PTXCONF_ARCH_ARM)    += linux-armv4
OPENSSL_ARCH-$(PTXCONF_ARCH_MIPS)   += linux-mipsel
OPENSSL_ARCH-$(PTXCONF_ARCH_SH_SH3) += linux-sh3
OPENSSL_ARCH-$(PTXCONF_ARCH_SH_SH4) += linux-sh4
else
OPENSSL_ARCH-$(PTXCONF_ARCH_ARM)    += linux-armeb
OPENSSL_ARCH-$(PTXCONF_ARCH_MIPS)   += linux-mips
OPENSSL_ARCH-$(PTXCONF_ARCH_SH_SH3) += linux-sh3eb
OPENSSL_ARCH-$(PTXCONF_ARCH_SH_SH4) += linux-sh4eb
endif

ifdef PTXCONF_OPENSSL
ifndef OPENSSL_ARCH-y
$(error *** Sorry unsupported ARCH in openssl.make)
endif
endif

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(OPENSSL_SOURCE):
	@$(call targetinfo)
	@$(call get, OPENSSL)



#
# autoconf
#
OPENSSL_PKGDIR = $(SYSROOT)
OPENSSL_CONF_OPT := \
	--prefix=/usr \
	--openssldir=/usr/lib/ssl \
	--install_prefix=$(OPENSSL_PKGDIR)

ifdef PTXCONF_OPENSSL_SHARED
OPENSSL_CONF_OPT += shared
else
OPENSSL_CONF_OPT += no-shared
endif

$(STATEDIR)/openssl.prepare:
	@$(call targetinfo)
	cd $(OPENSSL_DIR) && \
		$(OPENSSL_PATH) $(OPENSSL_CONF_ENV) \
		./Configure $(OPENSSL_ARCH-y) $(OPENSSL_CONF_OPT)
	@$(call touch)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/openssl.compile:
	@$(call targetinfo)
	cd $(OPENSSL_DIR) && $(OPENSSL_PATH) $(MAKE) $(OPENSSL_MAKE_VARS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/openssl.install:
	@$(call targetinfo)
	$(OPENSSL_PATH) make -C $(OPENSSL_DIR) install $(OPENSSL_CONF_ENV) $(OPENSSL_MAKE_VARS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/openssl.targetinstall:
	@$(call targetinfo)

	@$(call install_init, openssl)
	@$(call install_fixup, openssl,PACKAGE,openssl)
	@$(call install_fixup, openssl,PRIORITY,optional)
	@$(call install_fixup, openssl,SECTION,base)
	@$(call install_fixup, openssl,AUTHOR,"Marc Kleine-Budde <mkl@pengutronix.de>")
	@$(call install_fixup, openssl,DEPENDS,)
	@$(call install_fixup, openssl,DESCRIPTION,missing)

ifdef PTXCONF_OPENSSL_BIN
	@$(call install_copy, openssl, 0, 0, 0755, -, \
		/usr/bin/openssl)
endif
ifdef PTXCONF_OPENSSL_SHARED
	@$(call install_copy, openssl, 0, 0, 0644, $(OPENSSL_DIR)/libssl.so.1.0.0, /usr/lib/libssl.so.1.0.0)
	@$(call install_link, openssl, libssl.so.1.0.0, /usr/lib/libssl.so.0)
	@$(call install_link, openssl, libssl.so.1.0.0, /usr/lib/libssl.so)

	@$(call install_copy, openssl, 0, 0, 0644, $(OPENSSL_DIR)/libcrypto.so.1.0.0, /usr/lib/libcrypto.so.1.0.0)
	@$(call install_link, openssl, libcrypto.so.1.0.0, /usr/lib/libcrypto.so.0)
	@$(call install_link, openssl, libcrypto.so.1.0.0, /usr/lib/libcrypto.so)

endif
	@$(call install_finish, openssl)

	@$(call touch)

# vim: syntax=make
