# We provide this package
HOST_PACKAGES-$(PTXCONF_HOST_DEVICE_COMMON_DTBTOOL) += host-device-common-dtbtool

# Paths and names
HOST_DEVICE_COMMON_DTBTOOL_VERSION := 1.0
HOST_DEVICE_COMMON_DTBTOOL := \
	device/common/dtbtool-$(HOST_DEVICE_COMMON_DTBTOOL_VERSION)
# path to directory containing source files
HOST_DEVICE_COMMON_DTBTOOL_SRCDIR := \
	$(PTXDIST_WORKSPACE)/../opensource/device/common/dtbtool
HOST_DEVICE_COMMON_DTBTOOL_URL := file://$(HOST_DEVICE_COMMON_DTBTOOL_SRCDIR)
HOST_DEVICE_COMMON_DTBTOOL_DIR := $(HOST_BUILDDIR)/$(HOST_DEVICE_COMMON_DTBTOOL)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(STATEDIR)/host-device-common-dtbtool.get: $(host-device-common-dtbtool_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-device-common-dtbtool_extract: $(STATEDIR)/host-device-common-dtbtool.extract

$(STATEDIR)/host-device-common-dtbtool.extract: $(host-device-common-dtbtool_extract_deps_default)
	@$(call targetinfo, $@)
	mkdir -p $(HOST_DEVICE_COMMON_DTBTOOL_DIR)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------
HOST_DEVICE_COMMON_DTBTOOL_MAKEVARS += $(PARALLELMFLAGS)
HOST_DEVICE_COMMON_DTBTOOL_MAKE_ENV += CC=$(HOSTCC)

DTBTOOL_SRC = dtbtool.c
DTBTOOL_MK = $(HOST_DEVICE_COMMON_DTBTOOL_DIR)/Makefile
DTBTOOL_CFLAGS := \
	$(patsubst -Wno-unused-but-set-variable,,\
		$(patsubst -Werror,,$(QCT_CFLAGS)))

host-device-common-dtbtool_prepare: $(STATEDIR)/host-device-common-dtbtool.prepare

$(STATEDIR)/host-device-common-dtbtool.prepare: $(host-device-common-dtbtool_prepare_deps_default)
	@$(call targetinfo, $@)
	# dtbtool
	@echo 'CFLAGS += $(DTBTOOL_CFLAGS)' > $(DTBTOOL_MK)
	@echo '' >> $(DTBTOOL_MK)
	@echo 'vpath %.c $(HOST_DEVICE_COMMON_DTBTOOL_SRCDIR)' >> $(DTBTOOL_MK)
	@echo '' >> $(DTBTOOL_MK)
	@echo 'all: dtbtool' >> $(DTBTOOL_MK)
	@echo '' >> $(DTBTOOL_MK)
	@echo 'dtbtool: $(DTBTOOL_SRC)' >> $(DTBTOOL_MK)
	@echo '	$$(CC) $$(CFLAGS) $$(LDFLAGS) -o $$@ $$^ $$(LDLIBS)' >> $(DTBTOOL_MK)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

host-device-common-dtbtool_compile: $(STATEDIR)/host-device-common-dtbtool.compile

$(STATEDIR)/host-device-common-dtbtool.compile: $(host-device-common-dtbtool_compile_deps_default)
	@$(call targetinfo, $@)
	$(HOST_DEVICE_COMMON_DTBTOOL_MAKE_ENV) \
		$(HOST_DEVICE_COMMON_DTBTOOL_PATH) $(MAKE) -C \
		$(HOST_DEVICE_COMMON_DTBTOOL_DIR) \
		$(HOST_DEVICE_COMMON_DTBTOOL_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-device-common-dtbtool_install: $(STATEDIR)/host-device-common-dtbtool.install

$(STATEDIR)/host-device-common-dtbtool.install: $(host-device-common-dtbtool_install_deps_default)
	@$(call targetinfo, $@)
	@mkdir -p $(PTXCONF_SYSROOT_HOST)/bin
	@cp -f $(HOST_DEVICE_COMMON_DTBTOOL_DIR)/dtbtool \
		$(PTXCONF_SYSROOT_HOST)/bin/dtbtool
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------
host-device-common-dtbtool_clean: $(STATEDIR)/host-device-common-dtbtool.clean

$(STATEDIR)/host-device-common-dtbtool.clean:
	rm -rf $(STATEDIR)/host-device-common-dtbtool.*
	rm -rf $(HOST_DEVICE_COMMON_DTBTOOL_DIR)
