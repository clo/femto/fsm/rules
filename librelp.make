#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_LIBRELP) += librelp

#
# Paths and names
#
LIBRELP_VERSION	:= 1.0.0
LIBRELP		:= librelp-$(LIBRELP_VERSION)
LIBRELP_SUFFIX	:= tar.gz
LIBRELP_URL	:= http://download.rsyslog.com/librelp/$(LIBRELP).$(LIBRELP_SUFFIX)
LIBRELP_SOURCE	:= $(SRCDIR)/$(LIBRELP).$(LIBRELP_SUFFIX)
LIBRELP_DIR	:= $(BUILDDIR)/$(LIBRELP)
LIBRELP_LIBVER	:= 0.0.0

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------
librelp_get: $(STATEDIR)/librelp.get

$(STATEDIR)/librelp.get: $(librelp_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)


$(LIBRELP_SOURCE):
	@$(call targetinfo)
	@$(call get, LIBRELP)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------
#librelp_prepare: $(STATEDIR)/librelp.prepare

LIBRELP_PATH	:= PATH=$(CROSS_PATH)
LIBRELP_ENV 	:= $(CROSS_ENV)


# autoconf

LIBRELP_AUTOCONF := $(CROSS_AUTOCONF_USR)

$(STATEDIR)/librelp.prepare:
	@$(call targetinfo, $@)
	@$(call clean, $(LIBRELP_DIR)/config.cache)
	cd $(LIBRELP_DIR) && \
		$(LIBRELP_PATH) $(LIBRELP_ENV) \
		./configure $(LIBRELP_AUTOCONF)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/librelp.compile:
	@$(call targetinfo)
	cd $(LIBRELP_DIR) && $(LIBRELP_PATH) $(LIBRELP_ENV) $(MAKE) $(PARALLELMFLAGS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/librelp.install:
	@$(call targetinfo)
	@$(call install, LIBRELP)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/librelp.targetinstall:
	@$(call targetinfo)

	@$(call install_init, librelp)
	@$(call install_fixup, librelp,PACKAGE,librelp)
	@$(call install_fixup, librelp,PRIORITY,optional)
	@$(call install_fixup, librelp,VERSION,$(LIBRELP_VERSION))
	@$(call install_fixup, librelp,SECTION,base)
	@$(call install_fixup, librelp,AUTHOR,"Rainer Gerhards <rgerhards@adiscon.com>")
	@$(call install_fixup, librelp,DEPENDS,)
	@$(call install_fixup, librelp,DESCRIPTION,"Reliable Event Logging Protocol")

	@$(call install_copy, librelp, 0, 0, 0644, \
	 $(LIBRELP_DIR)/src/.libs/librelp.so.0.0.0, \
	 /usr/lib/librelp.so.0.0.0, y)
	@$(call install_link, librelp, librelp.so.0.0.0, /usr/lib/librelp.so.0)
	@$(call install_link, librelp, librelp.so.0.0.0, /usr/lib/librelp.so)

	@$(call install_finish, librelp)

	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

librelp_clean:
	rm -rf $(STATEDIR)/librelp.*
	rm -rf $(PKGDIR)/librelp_*
	rm -rf $(LIBRELP_DIR)

# vim: syntax=make
